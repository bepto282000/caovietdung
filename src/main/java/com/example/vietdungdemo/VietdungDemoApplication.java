package com.example.vietdungdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VietdungDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(VietdungDemoApplication.class, args);
    }

}
